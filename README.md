OpenAI API Wrapper for Advanced ChatGPT Development
===================================================

[中文说明](README_cn.md)

This Python package provides a comprehensive and efficient wrapper for the OpenAI API, designed to support advanced application development based on ChatGPT. The package simplifies the integration process and offers additional functionalities such as conversation management, model fine-tuning, retrieval of embeddings, automatic question generation, and offline model execution.

Changlogs
---------

v0.6.0
- Added ChatGPTBot and ChatGLMBot to support ChatGPT and ChatGLM backends, respectively.

Features
--------

* Provides a unified chatbot with large models such as ChatGPT and ChatGLM as the backend.
* Easy interaction with the OpenAI API for ChatGPT applications
* Conversation management for multiple chat sessions
* Support for fine-tuning the ChatGPT model
* Retrieval of embeddings for specific text passages
* Automatic question generation for given paragraphs
* Offline model execution with compatible models.

Installation
------------

You can install the package using pip:

`pip install openai-api-wrapper`

Quick Start
-----------

```python
# v0.6.0

from openai_api_wrapper.chatbots import ChatBot

# Initialize the Chatbot instance
# Set environment variable OPENAI_API_KEY to your OpenAI API key.
# Set environment variable OPENAI_PROXY to your OpenAI proxy setting, 
# such as "http://<proxy_server>:<proxy_port>" or "socks5://<proxy_server>:<proxy_port>" .
chatbot = ChatBot()
# or 
# chatbot = ChatBot(api_key="your_api_key", proxy="your_proxy")

reply_content= chatbot.ask(prompt)
```

Usage
-----

Here is a basic example of how to use the OpenAI API Wrapper:

```python
"""
v0.6.0
Set environment variable OPENAI_API_KEY to your OpenAI API key.
Set environment variable OPENAI_PROXY to your OpenAI proxy setting, 
such as "http://<proxy_server>:<proxy_port>" or "socks5://<proxy_server>:<proxy_port>" .
"""

from openai_api_wrapper.chatbots import ChatGPTBot
from openai_api_wrapper.chatbots import ChatGLMBot

def get_args():
    from argparse import ArgumentParser
    parser = ArgumentParser()

    parser.add_argument("--llm", type=str, default="chatglm", choices=["chatgpt", "chatglm"], help="The language model to use. (chatgpt, chatglm)")
    parser.add_argument("--model_name", type=str, default=None, help="The model name to use. (default: None).") 
    parser.add_argument("--temperature", type=float, default=0.95, help="The temperature to use. (default: 0.95).")
    parser.add_argument("--top_p", type=float, default=0.7, help="The top_p to use. (default: 0.7).")
    parser.add_argument("--proxy", type=str, default=None, help="The proxy to use. (default: None). You can set the environment variable OPENAI_PROXY to set the proxy.")
    parser.add_argument("--human", type=str, default=None, help="The human's name. (default: None)")
    parser.add_argument("--assistant", type=str, default=None, help="The assistant's name. (default: None)")
    parser.add_argument("--company", type=str, default=None, help="The company's name. (default: None)")

    args, unk_args = parser.parse_known_args()

    return args, unk_args

def main():
    args, _ = get_args()
    if args.llm == 'chatglm':
        chatbot = ChatGLMBot(human=args.human, assistant=args.assistant, company=args.company, \
            model_name=args.model_name,
            temperature=args.temperature,
            top_p=args.top_p,
            )
    elif args.llm == 'chatgpt':
        chatbot = ChatGPTBot( human=args.human, assistant=args.assistant, company=args.company, \
            proxy=args.proxy,
            model_name=args.model_name,
            temperature=args.temperature,
            top_p=args.top_p,
            )
    else:
        raise ValueError(f"Unknown language model: {args.llm}")


    response = chatbot.ask("你是谁？")
    print(f"{chatbot.assistant}: " + response)
    while True:
        prompt = input(f"{chatbot.human}: ")
        response = chatbot.ask(prompt)
        print(f"{chatbot.assistant}: " + response)

if __name__ == "__main__":
    main()
```

### ChatGPTBot Output
> **Assistant**:
> 我是由OpenAI训练的大型语言模型，可以回答各种问题，提供深入的解释和讨论，从简单问题到复杂问题都可以应对。我能够生成类似于人类的文本，可以进行自然对话，并提供与话题相关的连贯和有意义的回答。我的能力不断提高和发展，可以处理和理解大量的文本，并根据输入生成自己的文本，可以涉及各种话题。如果我没有接受过某个特定话题的培训，我将无法回答相关问题，但如果我接受过相关培训，我将能够提供与所接受的信息一致的回答。总的来说，我是一个强大的工具，可以帮助各种任务，并提供有价值的见解和信息。无论您需要帮助解决具体问题，还是只想谈论特定话题 ，我都可以为您提供帮助。
>
> **Human**: 经常失眠怎么办？
>
> **Assistant**: 失眠是指无法入睡或难以保持睡眠的情况。以下是一些可能有助于缓解失眠的方法：
>
> 1. 维持规律的睡眠时间表：尽量在每天相同的时间入睡和起床，即使在周末也要尽量保持相同的时间。
> 
> 2. 创造一个舒适的睡眠环境：保持卧室安静、黑暗、凉爽和通风良好。
> 
> 3. 避免在睡前进行刺激性活动：如看电视、使用电子设备或进行激烈的运动。
> 
> 4. 放松身体和思想：可以试着进行深呼吸、渐进性肌肉松弛或冥想等放松技巧。
> 
> 5. 避免过度饮食和饮用咖啡因和酒精：咖啡因和酒精会影响睡眠质量。
> 
> 6. 考虑使用草药或药物：有些草药和药物可以帮助缓解失眠，但应在医生的指导下使用。
> 
> 如果您的失眠持续时间较长或影响到您的日常生活，请及时咨询医生。
>
> **Human**: 请给一份锻炼身体的日程安排表，用Markdown语法输出。
>
> **Assistant**:
> | 时间 | 锻炼内容 |
> | ---- | -------- |
> | 早上 | 晨跑     |
> | 中午 | 散步     |
> | 晚上 | 游泳     |
> 
> 注意：锻炼计划应根据个人身体状况和时间安排进行调整，适度运动才是最好的。
>
> **Human**: 请详细一些，加上时间。
> 
> **Assistant**: 好的，以下是更详细的锻炼日程安排表：
>
> | 时间 | 锻炼内容                     |
> | ---- | ------------------------ |
> | 早上 | 6:00-7:00 晨跑或者瑜伽 |
> | 中午 | 12:00-12:30 散步或者慢跑 |
> | 晚上 | 19:00-20:00 游泳或者健身房 |
> 
> 需要注意的是，锻炼计划应该根据个人身体状况和时间安排进行调整，适度运动才是最好的。


### ChatGLMBot Output
> **Assistant**:
> 我是一个大型语言模型，由THUDM训练。我能够协助完成广泛的任务，从回答简单的问题到提供深入解释和讨论，涉及广泛的主题。作为一个语言模型，我能够生成人类般的文本，根据输入产生自然流畅的对话，并提供有关特定主题的准确和有用的信息。我不断学习和改进，其能力也在不断进化。我能够处理和理解大量文本，使用这些信息回答各种问题，并提供有关各种主题的准确和 informative 的回答。我的回答必须与我已经训练的信息保持一致。例如，如果我被询问一个与我尚未训练的主题相关的问题，我将不能提供回答。但是，如果我被询问一个与我已训练的主题相关的问题，我将能够提供与训练信息一致的回答。总之，我是一个功能强大的工具，能够协助完成广泛的任务，提供有关广泛主题的有价值的信息和 insights。无论需要帮助回答特定问题或只是想与特定主题进行讨论，我都会尽力协助。
>
> **Human**: 经常失眠怎么办？
>
> **Assistant**:
> 失眠可能是由于许多不同的原因引起的，例如焦虑、压力、抑郁、生活习惯、饮食习惯等等。以下是一些可能有助于缓解失眠的建议：
>
> 1. 制定规律的睡眠时间表：保持每天相同的起床时间和睡眠时间，即使在周末和假期也要尽量保持相同的时间。
>
> 2. 创造一个舒适的睡眠环境：保持房间安静、凉爽、黑暗、舒适和整洁。
> 
> 3. 避免在睡觉前使用电子设备：手机、电脑、电视和电子设备会刺激大脑，影响睡眠。
> 
> 4. 避免过度饮食和饮料：避免在睡觉前吃大量的食物和饮料，特别是咖啡、茶、巧克力和酒精。
> 
> 5. 进行放松活动：例如冥想、深呼吸、瑜伽或温水泡澡等，放松身心，减轻压力和焦虑。
> 
> 6. 寻求帮助：如果失眠问题持续存在或影响日常生活，建议寻求医生或心理医生的帮助，他们可以提供有效的治疗和建议。
>
>希望这些建议能帮助缓解失眠问题。如果需要更多的帮助或建议，请随时与医生或心理医生联系。
>
> **Human**: 请给一份锻炼身体的日程安排表，用Markdown语法输出。
>
> **Assistant**: 以下是一份锻炼身体的日程安排表，使用Markdown语法输出：
>
> ```
> | 时间   | 活动      |
> | -------- | -------- |
> | 早上 | 慢跑      |
> | 上午 | 俯卧撑   |
> | 中午 | 仰卧起坐 |
> | 下午 | 拉伸        |
> | 晚上 | 散步      |
> ```
> 
> 请注意，这只是一个示例。可以根据自己的时间表和健身计划来定制自己的锻炼日程安排。同时，请确保在锻炼时保持适当的休息和饮食，以 帮助身体恢复和增强肌肉。
>
> **Human**: 请详细一些，加上时间。
>
> **Assistant**:
> 以下是一份更详细的锻炼身体的日程安排表，加上了每个活动的具体时间：
>
> ```
> | 时间   | 活动      |
> | -------- | -------- |
> | 早上 | 慢跑1小时，做3组，每组10分钟 |
> | 上午 | 俯卧撑10组，每组20-30次 |
> | 中午 | 仰卧起坐10组，每组20-30次 |
> | 下午 | 拉伸15分钟 |
> | 晚上 | 散步30分钟 |
> ```
> 
> 请注意，这只是一个示例。可以根据自己的时间表和健身计划来定制自己的锻炼日程安排。同时，请确保在锻炼时保持适当的休息和饮食，帮 助身体恢复和增强肌肉。
>
> 希望这份日程安排表能帮助到您！

For a more detailed example with all the available features, check out the `chatbots/chatbot.py` file in the repository.

Documentation
-------------

You can find the complete documentation for this package in the `docs` folder, or check out the source code for more details on the implementation.

Roadmap
-------

We plan to continually improve and expand the functionality of this package. Some of the upcoming features include:

* Integration with various machine learning frameworks
* Support for multi-modal inputs (e.g., text, images, audio)
* Expansion of available pre-trained models
* Simplified deployment options for various platforms

Contributing
------------

We welcome contributions from the community! If you'd like to contribute, please follow these steps:

1. Fork the repository
2. Create a new branch for your changes (`git checkout -b my-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push the branch (`git push origin my-feature`)
5. Create a new pull request

Please make sure to add tests and documentation for any new features or changes.

License
-------

This project is licensed under the MIT License. See the `LICENSE` file for more details.
